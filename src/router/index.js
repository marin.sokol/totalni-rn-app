import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import Player from '../screens/Player';
// import Content from '../screens/Content';
import About from '../screens/About';

export default createBottomTabNavigator(
  {
    Radio: Player,
    Kontakt: About,
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) => { //eslint-disable-line
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Radio') {
          iconName = 'ios-play';
        } else if (routeName === 'Kontakt') {
          iconName = 'ios-chatbubbles';
        }

        return <Icon name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'white',
      inactiveTintColor: 'gray',
      style: {
        backgroundColor: '#404041',
      },
    },
  },
);
