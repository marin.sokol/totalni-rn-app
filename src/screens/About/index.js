import React, { PureComponent } from 'react';
import {
  Text,
  View,
  StatusBar,
  Platform,
  Linking,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import IconFa from 'react-native-vector-icons/FontAwesome';
import call from 'react-native-phone-call';

import styles from './styles';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const logo = require('../../assets/partyhitsradio-logo.png');

export default class Content extends PureComponent {
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={{ height: STATUSBAR_HEIGHT, backgroundColor: '#241f20' }}>
          <StatusBar translucent backgroundColor="#241f20" barStyle="light-content" />
        </View>
        <View style={styles.header}>
          <Image
            source={logo}
            style={{
              width: Dimensions.get('window').width * 0.4,
              height: Dimensions.get('window').height * 0.2,
              resizeMode: 'contain',
            }}
          />
        </View>
        <View style={styles.headerBottom}>
          <Text style={styles.headerText}>KONTAKT & INFO</Text>
        </View>
        <View style={styles.title}>
          <Text>
            Brzi linkovi
          </Text>
        </View>
        <View>
          <View>
            <TouchableOpacity onPress={() => call({ number: '0800805822', prompt: false })} style={styles.listItem}>
              <Icon name="ios-call" size={40} color="black" style={styles.socialIcon} />
              <View>
                <Text>BESPLATAN TELEFON</Text>
                <Text style={styles.subTitle}>0800 805 822</Text>
              </View>
              <Icon name="ios-arrow-forward" size={20} color="grey" style={styles.arrow} />
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={() => Linking.openURL('mailto:info@ultrasplit.hr')} style={styles.listItem}>
              <Icon name="ios-mail" size={40} color="black" style={styles.socialIcon} />
              <View>
                <Text>E-MAIL</Text>
                <Text style={styles.subTitle}>info@ultrasplit.hr</Text>
              </View>
              <Icon name="ios-arrow-forward" size={20} color="grey" style={styles.arrow} />
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={() => Linking.openURL('http://www.ultrasplit.hr')} style={styles.listItem}>
              <Icon name="ios-globe" size={40} color="black" style={styles.socialIcon} />
              <View>
                <Text>WEB</Text>
                <Text style={styles.subTitle}>www.ultrasplit.hr</Text>
              </View>
              <Icon name="ios-arrow-forward" size={20} color="grey" style={styles.arrow} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.title}>
          <Text>
            Društvene mreže
          </Text>
        </View>
        <View>
          <View>
            <TouchableOpacity onPress={() => Linking.openURL('https://www.facebook.com/ultrasplit.hr/')} style={styles.listItem}>
              <IconFa name="facebook" size={40} color="#3c5898" style={styles.socialIcon} />
              <Text>FACEBOOK</Text>
              <Icon name="ios-arrow-forward" size={20} color="grey" style={styles.arrow} />
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={() => Linking.openURL('https://www.instagram.com/ultra_split/')} style={styles.listItem}>
              <IconFa name="instagram" size={40} color="#dd2a7b" style={styles.socialIcon} />
              <Text>INSTAGRAM</Text>
              <Icon name="ios-arrow-forward" size={20} color="grey" style={styles.arrow} />
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={() => Linking.openURL('viber://send?phone=385911805822')} style={styles.listItem}>
              <IconFa name="phone-square" size={40} color="#7b519d" style={styles.socialIcon} />
              <Text>VIBER</Text>
              <Icon name="ios-arrow-forward" size={20} color="grey" style={styles.arrow} />
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={() => Linking.openURL('whatsapp://send?phone=385911805822')} style={styles.listItem}>
              <IconFa name="whatsapp" size={40} color="#65d467" style={styles.socialIcon} />
              <Text>WHATSAPP</Text>
              <Icon name="ios-arrow-forward" size={20} color="grey" style={styles.arrow} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.title}>
          <Text>
            Info
          </Text>
        </View>
        <View>
          <View>
            <TouchableOpacity onPress={() => Linking.openURL('http://propono.hr/Ultra/privacy_policy_ultra.html')} style={styles.listItem}>
              <Text>Privacy Policy</Text>
              <Icon name="ios-arrow-forward" size={20} color="grey" style={styles.arrow} />
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={() => Linking.openURL('http://propono.hr')} style={styles.listItem}>
              <Text>Developed by Propono Multimedia</Text>
              <Icon name="ios-arrow-forward" size={20} color="grey" style={styles.arrow} />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}
