import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#231f20',
  },
  header: {
    backgroundColor: '#231f20',
    alignItems: 'center',
  },
  headerBottom: {
    backgroundColor: '#231f20',
    alignItems: 'center',
    padding: 10,
  },
  headerText: {
    fontFamily: 'Celdum',
    fontWeight: 'bold',
    color: 'white',
    fontSize: 28,
  },
  listItem: {
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 5,
    borderStyle: 'solid',
    backgroundColor: 'white',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  subTitle: {
    fontFamily: 'Celdum',
    color: 'grey',
    fontSize: 14,
  },
  socialIcon: {
    marginRight: 10,
    minWidth: 40,
    textAlign: 'center',
  },
  arrow: {
    position: 'absolute',
    right: 15,
  },
  title: {
    backgroundColor: '#ccc',
    padding: 5,
  },
});
