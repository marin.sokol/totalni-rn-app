import React, { PureComponent } from 'react';
import {
  Text,
  View,
  ListView,
  ActivityIndicator,
} from 'react-native';

import styles from './styles';
import CONFIG from '../../config';

export default class Content extends PureComponent {
  state = {
    news: [],
    loading: true,
  }

  componentWillMount() {
    fetch('http://c5.hostingcentar.com:8061/played.html?sid=1')
      .then(response => console.log(response))
      .catch(err => console.log(err)); // eslint-disable-line

    fetch(CONFIG.api)
      .then(response => response.json())
      .then(res => this.setState({
        loading: false,
        news: this.getDataSource().cloneWithRows(res.results),
      }))
      .catch(err => console.log(err)); // eslint-disable-line
  }

  getDataSource = () => new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  renderRow = data => <Text>{data.name}</Text>

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <View style={styles.loading}>
            <ActivityIndicator />
          </View>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <ListView
          dataSource={this.state.news}
          renderRow={this.renderRow}
        />
      </View>
    );
  }
}
