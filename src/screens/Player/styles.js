import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#231f20',
    justifyContent: 'center',
    alignItems: 'center',
  },
  background: {
    // backgroundColor: '#ccc',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  play: {
    backgroundColor: '#ed1654',
    width: 130,
    height: 130,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 9999,
  },
  nowPlaying: {
    fontFamily: 'Celdum',
    marginTop: 50,
    color: '#ed1654',
  },
  songContainer: {
    minWidth: '65%',
    marginTop: 5,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 10,
    paddingRight: 10,
    borderStyle: 'solid',
    borderBottomColor: '#fff',
    borderBottomWidth: 1,
    borderTopColor: '#fff',
    borderTopWidth: 1,
    alignItems: 'center',
  },
  artistName: {
    fontFamily: 'Celdum',
    opacity: 0.8,
    color: 'white',
  },
  songName: {
    fontFamily: 'Celdum',
    color: 'white',
    // fontWeight: 'bold',
  },
});
