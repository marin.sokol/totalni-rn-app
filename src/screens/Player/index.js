import React, { PureComponent } from 'react';
import {
  NativeModules,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  SafeAreaView,
  Image,
  Dimensions,
  Platform,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from './styles';
import CONFIG from '../../config';

const { ReactNativeAudioStreaming } = NativeModules;

const logo = require('../../assets/logo.jpg');
const background = require('../../assets/background.jpg');

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export default class Player extends PureComponent {
  state = {
    playing: true,
    currentSong: '',
  }

  componentDidMount() {
    this.fetchCurrentSong();
    this.interval = setInterval(this.fetchCurrentSong, 5 * 1000);

    ReactNativeAudioStreaming.play(CONFIG.streamUrl, {
      showIniOSMediaCenter: true,
      showInAndroidNotifications: true,
    });
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  fetchCurrentSong = () =>
    fetch(`${CONFIG.streamUrl}currentsong?sid=1`)
      .then(response => this.setState({ currentSong: response._bodyText }))
      .catch(err => console.log(err)); // eslint-disable-line

  handlePlay = () => {
    const { playing } = this.state;
    if (playing) {
      ReactNativeAudioStreaming.stop();
    } else {
      ReactNativeAudioStreaming.play(CONFIG.streamUrl, {
        showIniOSMediaCenter: true,
        showInAndroidNotifications: true,
      });
    }
    this.setState({ playing: !playing });
  }

  render() {
    const { playing, currentSong } = this.state;

    const index = currentSong.indexOf(' - ');
    const artistName = currentSong.substr(0, index);
    const songName = currentSong.substr(index + 1).replace('- ', '');

    const playButton = playing ? 'ios-pause' : 'ios-play';

    return (
      <SafeAreaView style={styles.container}>

        <View style={{ height: STATUSBAR_HEIGHT, backgroundColor: '#231f20' }}>
          <StatusBar translucent backgroundColor="#231f20" barStyle="light-content" />
        </View>

        <Image
          source={logo}
          style={{
            width: Dimensions.get('window').width * 0.6,
            height: Dimensions.get('window').width * 0.3,
            resizeMode: 'contain',
            marginBottom: 20,
          }}
        />
        <View style={{ width: '100%', height: Dimensions.get('window').width * 0.55 }}>
          <ImageBackground
            style={styles.background}
            resizeMode="center"
            source={background}
          >
            <TouchableOpacity onPress={this.handlePlay} style={styles.play}>
              <Icon name={playButton} size={80} color="white" style={{ backgroundColor: 'transparent' }} />
            </TouchableOpacity>
          </ImageBackground>
        </View>
        <View>
          <Text style={styles.nowPlaying}>NOW PLAYING</Text>
        </View>
        <View style={styles.songContainer}>
          <Text style={styles.artistName}>
            {artistName.toUpperCase()}
          </Text>
          <Text style={styles.songName}>
            {songName}
          </Text>
        </View>
      </SafeAreaView>
    );
  }
}
