# TotalniFM - streaming app

This is React Native app built with `react-native init`.

## Installation
```bash
$ git clone https://gitlab.com/marin.sokol/totalni-rn-app <my-project-name>
$ cd <my-project-name>
```
When that's done, install the project dependencies. It is recommended that you use [Yarn](https://yarnpkg.com/) for deterministic dependency management, but `npm install` will suffice.

```bash
$ yarn  # Install project dependencies (or `npm install`)
```
## Running the Project
```bash
$ yarn start  # Start the development server (or `npm start`)
```
|`yarn <script>`    |Description|
|-------------------|-----------|
|`lint`             |[Lints](http://stackoverflow.com/questions/8503559/what-is-linting) the project for potential errors|
|`lint:fix`         |Lints the project and [fixes all correctable errors](http://eslint.org/docs/user-guide/command-line-interface.html#fix)|

## AUDIO STREAMING
Npm package [react-native-audio-streaming](https://github.com/tlenclos/react-native-audio-streaming) was added to project to get background streaming of audio. We used 2. option for IOS installation. 

Also, we used a bit different code when explained in repo. 

```
import { NativeModules } from 'react-native';

const { ReactNativeAudioStreaming } = NativeModules;

const url = "http://lacavewebradio.chickenkiller.com:8000/stream.mp3";
ReactNativeAudioStreaming.pause();
ReactNativeAudioStreaming.resume();
ReactNativeAudioStreaming.play(url, {showIniOSMediaCenter: true, showInAndroidNotifications: true});
ReactNativeAudioStreaming.stop();
```